'use strict';
const { Model } = require('sequelize');
const { hashSync } = require('bcryptjs')
require('dotenv').config()
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  user.init({
    uid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'UID already in use!'
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail:true
      },
      unique: {
        args: true,
        msg: 'Email address already in use!'
      }
    },
    name: DataTypes.STRING,
    pass: {
      type: DataTypes.TEXT('medium'),
      allowNull: false,
    },
    sex: DataTypes.CHAR(1),
    token: {
      type: DataTypes.TEXT,
      /* set(value) {
        this.setDataValue('token',hashSync(genSaltSync(25),process.env.APP_SALT))
      } */
    }
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};