const { user } = require("./../models")
const { genSaltSync, hashSync, compareSync } = require("bcryptjs")
const self = {}

self.login = async (req,res) => {
    try{
        const body = req.body
        const userlogin = await user.findOne({
            // attributes:["id","email","name","sex","token","createdAt"],
            where:{
                email:body.email
            }
        });
        if(userlogin) {
            const checkpass = compareSync(body.password.toString(), userlogin.pass)
            if(checkpass) {
                delete userlogin.pass;
                delete userlogin.updatedAt;
                res.status(200).json({
                    status:"success",
                    message:"Successfully Login",
                    data: {
                        token: Buffer.from(userlogin.token).toString('base64'),
                        email: userlogin.email,
                        name: userlogin.name
                    }
                });  
            }

            res.status(200).json({
                status:"error",
                message:"Wrong User or Password"
            });
        }
        
        res.status(200).json({
            status:"error",
            message:"User Not Found"
        })
    }catch(error){
        res.status(500).json({
            status:"error",
            data:error.toString()
        })
    }
}
self.logout = async (req, res) => {
    let token = req.get('authorization');
    token = token.slice(7);
    token = Buffer.from(token, 'base64').toString('utf8')
    const userlogin = await user.findOne({
        where:{
            token:token
        }
    });
    // generate new token
    try {
        const random = genSaltSync(10);
        userlogin.token = hashSync(random,11)
        await userlogin.save()
        res.status(200).json({
            status: "success",
            message: "Your Successfully Logout"
        })
    } catch(error) {
        res.status(500).json({
            status: "failed",
            message: "Failed to Logout"
        })
    }
    
}

module.exports = self;
