const { user, Sequelize } = require("./../models");
const { Op, or, and } = Sequelize;
const { genSaltSync, hashSync } = require('bcryptjs')
const { v4: uuidv4 } = require('uuid');
require('dotenv').config()
const self = {};

self.getAll = async (req,res) => {
    try{
        let data = await user.findAll({
            attributes:["uid","email","name","sex","createdAt"]
        });
        return res.json({
            status:"success",
            data:data
        })
    }catch(error){
        res.status(500).json({
            status:"error",
            data:error
        })
    }
}
self.getById = async (req,res) => {
    try{
        let id = req.params.id;
        let data = await user.findOne({
            attributes:["uid","email","name","sex","createdAt"],
            where:{
                uid:id
            }
        });
        return res.json({
            status:"success",
            data:data
        })
    }catch(error){
        res.status(500).json({
            status:"error",
            data:error
        })
    }
}
self.getByEmail = async (req,res) => {
    try{
        const id = req.params.email;
        const data = await user.findOne({
            attributes:["uid","email","name","sex","createdAt"],
            where:{
                email:id
            }
        });
        return res.json({
            status:"success",
            data:data
        })
    }catch(error){
        res.status(500).json({
            status:"error",
            data:error
        })
    }
}
self.getByToken = async (token) => {
    try{
        const data = await user.findOne({
            attributes:["uid","email","name","sex","createdAt"],
            where:{
                token: token
            }
        });
        return true
    }catch(error){
        throw error
    }
}
self.search = async (req,res) => {
    try{
        let text = req.query.text;
        let data = await user.findAll({
            attributes:["uid","name","email","sex"],
            where:{
                name:{
                    [Op.like]:"%"+text+"%"
                }
            }
        });
        return res.json({
            status:"ok",
            data:data
        })
    }catch(error){
        res.status(500).json({
            status:"error",
            data:error
        })
    }
}
self.save = async (req,res) => {
    try{
        const body = req.body;
        if(body.password) {
            body.pass = hashSync(body.password,10)
            body.token = hashSync(process.env.APP_SALT+body.pass+genSaltSync(8),11);
            body.uid = uuidv4();
        }
        const data = await user.create(body);
        return res.status(201).json({
            status:"success",
            data: { id: data.uid },
            message:"Successfully Created User"
        })
    }catch(error){
        res.status(500).json({
            status:"error",
            data:error.toString()
        })
    }
}
self.update = async (req,res) => {
    try{
        let id = req.params.id;
        let body = req.body;
        let data = await user.update(body,{
            where:{
                uid:id
            }
        });
        return res.json({
            status:"success",
            message: "Change User Successfully"
        })
    }catch(error){
        res.status(500).json({
            status:"error",
            data:error
        })
    }
}
self.delete = async (req,res) => {
    try{
        let id = req.params.id;
        let data = await user.destroy({
            where:{
                uid:id
            }
        });
        return res.json({
            status:"success",
            data:data
        })
    }catch(error){
        res.status(500).json({
            status:"error",
            data:error
        })
    }
}

module.exports = self;