const express = require('express')
const cors = require('cors')
const app = express();
const corsOptions = {
    origin: '*'
}
// import module router
const userRouter = require ('./routers/module/user.router')
const loginRouter = require ('./routers/module/login.router')
//middleware
app.use(cors(corsOptions))
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// routes
app.get("/", (req, res) => {
    res.send("Pintap Test APP.<br/>Go To <a href='/ping'><b>/ping</b></a> to test API")
})
app.get("/ping", (req,res) => {
    const token = req.get('authorization');
    if(token) {
        res.status(200).json({
            message: "API READY",
            "with-token": true,
        })
    }
    res.status(200).json({
        message: 'API READY',
        application: 'For Pintap Test'
    });
})
app.use("/api", loginRouter);
app.use("/api/users", userRouter);

module.exports = app;
