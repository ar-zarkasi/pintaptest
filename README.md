# Arfan Zarkasi Pintap Code Test
## _Node JS - AWS Lambda_
### cli using
- sequelize-cli
- serverless
- node v14.x with npx
- after clone, dont forget to `npm install` and make your .env file (copy from .env.example file)

### Setup Database
- Run Your Database Docker (this case using mysql 5.7)
```sh
docker run --name name-some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pswd -d mysql:5.7
```
- Edit Your .env file for database credential & Save
- Running migration by execute:
```sh
npm run initiate
```
After migration you can running app for 2 option serverless or local:
- for running local, execute:
```sh
npm run start
```
and try to running http://localhost:3000 via Postman
- For Running serverless
```sh
npm run deploy
```

#### For Test
- You can using this for example test
>> GET /ping
- url with format request body json
>> POST /api/login
example: { "email":"admin@test.com", "password":"123456" }
- this url must with Authorization bearer token
>> POST /api/logout 
GET /api/users
GET /api/users/{id}
POST /api/users
PUT /api/users/{id}
DELETE /api/users/{id}

```sh
example POST /api/users body: 
{
    "email":"xxxxxxxxxxx",
    "name": "user-test",
    "password": "for-password",
    "sex": "M or F"
}
```

