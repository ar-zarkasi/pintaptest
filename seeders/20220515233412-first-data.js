'use strict';
const { genSaltSync, hashSync } = require('bcryptjs')
const { v4: uuid } = require('uuid');

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return await queryInterface.bulkInsert('users', [
        {
         id: 1,
         uid: uuid(),
         email: 'admin@test.com',
         name: 'Administrator',
         pass: hashSync('123456',10),
         sex: 'M',
         token: hashSync(genSaltSync(11),10),
         createdAt: new Date(),
        },
        {
          id: 2,
          uid: uuid(),
          email: 'example@test.com',
          name: 'User 1',
          pass: hashSync('qwerty123',10),
          sex: 'F',
          token: hashSync(genSaltSync(11),10),
          createdAt: new Date(),
         },
         {
          id: 3,
          uid: uuid(),
          email: 'example2@test.com',
          name: 'User 2',
          pass: hashSync('asdfgh456',10),
          sex: 'M',
          token: hashSync(genSaltSync(11),10),
          createdAt: new Date(),
         },
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete('users', null, {});
  }
};
