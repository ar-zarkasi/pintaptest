const { user } = require("./../models")
const action = {}
action.checkToken = async (req, res, next) => {
    try {
        let token = req.get('authorization');
        if (token) {
            token = token.slice(7);
            token = Buffer.from(token, 'base64').toString('utf8')
            const verify = await user.findOne({
                where:{
                    token: token
                }
            });
            if(verify) {
                next()
                return true;
            }
            res.status(401).json({
                status: false,
                message: "Invalid Authorization"
            })
        } else {
            res.status(401).json({
                status: "error",
                message: "Access Denied unauthorized User"
            });
        }
    } catch (error) {
        res.status(500).json({
            status: "error",
            error: error.toString()
        });
    }
    
}

module.exports = action
