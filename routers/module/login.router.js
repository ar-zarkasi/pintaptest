const router = require("express").Router();
const loginController = require('../../controllers/loginController')
const { checkToken } = require("../../auth");

router.post("/login",loginController.login)
router.post("/logout", checkToken, loginController.logout)

module.exports = router
