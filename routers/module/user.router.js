const userController = require("../../controllers/userController");
const router = require("express").Router();
const { checkToken } = require("../../auth");

router.post("/", checkToken, userController.save);
router.get("/", checkToken, userController.getAll);
router.get("/:id", checkToken, userController.getById);
router.put("/:id", checkToken, userController.update);
// router.post('/login', login);

module.exports = router;
